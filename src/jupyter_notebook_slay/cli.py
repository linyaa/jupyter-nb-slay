# Copyright 2024 Lina Versace <lina@kiwitre.net>
# SPDX-License-Identifier: MIT

import click

@click.command()
def main():
    print("hello")

if __name__ == "__main__":
    main()
