# jupyter-notebook-slay

A tool for [Jupyter notebooks][].

[Jupyter notebooks]: https://docs.jupyter.org/en/latest/#what-is-a-notebook

## License

See [LICENSE](./LICENSE) file.
